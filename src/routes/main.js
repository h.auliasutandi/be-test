const express = require("express");
const router = express.Router();

const verifyToken = require("../middleware/auth");
const { loginAuth, registerAuth, getUserInfo } = require("../controllers/authController");
const { createCourier, getCourierByOriginAndDestination } = require("../controllers/courierController");

// main
router.get("/", (req, res) => {
    res.send("Hello , Ecommerce API for Admin is Running !");
});

router.post("/login", loginAuth);
router.post("/register", registerAuth);
router.get("/getuser",verifyToken ,getUserInfo);

router.post('/courier',verifyToken, createCourier )
router.get('/courier', verifyToken, getCourierByOriginAndDestination);

module.exports = router