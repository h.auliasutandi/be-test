/*
  Warnings:

  - You are about to drop the column `email` on the `User` table. All the data in the column will be lost.
  - You are about to drop the `Checklist` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[msisdn]` on the table `User` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `msisdn` to the `User` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX `User_email_key` ON `User`;

-- AlterTable
ALTER TABLE `User` DROP COLUMN `email`,
    ADD COLUMN `msisdn` INTEGER NOT NULL,
    ADD COLUMN `name` VARCHAR(191) NULL,
    MODIFY `username` VARCHAR(191) NULL;

-- DropTable
DROP TABLE `Checklist`;

-- CreateTable
CREATE TABLE `Courier` (
    `id` VARCHAR(191) NOT NULL,
    `logistic_name` VARCHAR(191) NOT NULL,
    `amount` INTEGER NOT NULL,
    `destination_name` VARCHAR(191) NOT NULL,
    `origin_name` VARCHAR(191) NOT NULL,
    `duration` VARCHAR(191) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE UNIQUE INDEX `User_msisdn_key` ON `User`(`msisdn`);
