const { PrismaClient } = require("@prisma/client");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();

const prisma = new PrismaClient();

const loginAuth = async (req, res) => {
	const { msisdn, password } = req.body;
	
	try {
		const user = await prisma.Users.findUnique({
			where: {
				msisdn
			}
		});
		
		let passwordIsValid = bcrypt.compareSync(password, user.password);
		
		if (!passwordIsValid) {
			return res.status(401).json({ msg: "login failed , wrong credentials" });
		}

		jwt.sign({ id: user.id, msisdn: user.msisdn }, process.env.TOKEN_SECRET, { expiresIn: 60 * 60 * 24 }, (err, token) => {
			res.status(200).json({
				msg: "login success",
				data: {
					id: user.id,
					msisdn: user.msisdn,
					name: user.name,
					username: user.username,
				},
				token,
			});
		});
	} catch (err) {
		res.status(400).json({ msg: "bad request" });
	}
};

const registerAuth = async (req, res) => {
	const { msisdn,name,username, password } = req.body;
	let hashed = bcrypt.hashSync(password, 8);

    if (!msisdn.startsWith("62")) {
        return res.status(400).json({ msg: "MSISDN must start with '62'" });
    }
    if (!username || typeof username !== 'string') {
        return res.status(400).json({ msg: "Invalid username" });
    }

	try {
        const existingMsisdnUser = await prisma.Users.findUnique({ where: { msisdn } });
        if (existingMsisdnUser) {
            return res.status(400).json({ msg: "MSISDN already exists" });
        }

        const existingUsernameUser = await prisma.Users.findUnique({ where: { username } });
        if (existingUsernameUser) {
            return res.status(400).json({ msg: "Username already exists" });
        }
        
		const user = await prisma.Users.create({
			data: {
				msisdn,
                name,
                username,
				password: hashed,
			},
		});
		jwt.sign({ id: user.id, msisdn: user.msisdn }, process.env.TOKEN_SECRET, { expiresIn: 60 * 60 * 24 }, (err, token) => {
			res.status(200).json({
				msg: "Account successfuly crated",
				data: {
					id: user.id,
					msisdn: user.msisdn,
					name: user.name,
					username: user.username,
				},
				token,
			});
		});
	} catch (err) {
		res.status(400).json({ msg: "bad request" });
	}
};

const getUserInfo = async (req, res) => {
    try {
		if (!req.user) {
            return res.status(401).json({ error: 'User information not found' });
        }

		const user = await prisma.Users.findUnique({
			where: {
				msisdn: req.user.msisdn
			}
		});

		res.status(200).json({
			msg: "get Account successfuly",
			data: {
				id: user.id,
				msisdn: user.msisdn,
				name: user.name,
				username: user.username,
			},
		});
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Failed to fetch user information' });
    }
};


module.exports = {
	loginAuth,
	registerAuth,
	getUserInfo
};
