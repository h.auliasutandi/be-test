const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

const createCourier = async (req, res) => {
    try {
        let { logistic_name, amount, destination_name, origin_name, duration } = req.body;

        if (!logistic_name || !amount || !destination_name || !origin_name || !duration) {
            return res.status(400).json({ error: 'All fields are required' });
        }

        if (typeof amount !== 'number') {
            return res.status(400).json({ error: 'Amount must be a number' });
        }

        origin_name = origin_name.toUpperCase();
        destination_name = destination_name.toUpperCase();

        const existingCourier = await prisma.courier.findFirst({
            where: {
                logistic_name,
                amount,
                destination_name,
                origin_name,
                duration
            }
        });

        if (existingCourier) {
            return res.status(400).json({ error: 'Courier with the same parameters already exists' });
        }

        const courier = await prisma.courier.create({
            data: {
                logistic_name,
                amount,
                destination_name,
                origin_name,
                duration
            }
        });
        res.status(201).json({ message: 'Courier added successfully', courier });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Failed to add courier' });
    }
}

const getCourierByOriginAndDestination = async (req, res) => {
    try {
        let { origin_name, destination_name } = req.body;
        if (!origin_name || !destination_name) {
            return res.status(400).json({ error: 'Both origin_name and destination_name are required' });
        }
        
        origin_name = origin_name.toUpperCase();
        destination_name = destination_name.toUpperCase();
        console.log(origin_name,destination_name,'ini data origin destination')
        const courier = await prisma.courier.findMany({
            where: {
                AND: [
                    { origin_name: { equals: origin_name } },
                    { destination_name: { equals: destination_name } }
                ]
            }
        });

        if (courier.length === 0) {
            return res.status(404).json({ message: 'Courier not found for the given origin and destination' });
        }

        res.status(200).json(courier);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Failed to fetch courier data' });
    }
}

module.exports = {
    createCourier,
    getCourierByOriginAndDestination
};
