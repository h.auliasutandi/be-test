const express = require('express')
const dotenv = require('dotenv')
const router = require('./src/routes/main')
const swaggerUI = require('swagger-ui-express')
const swaggerDocument = require('./src/docs/swagger-api-docs.json');

const cors = require('cors')

var corsOptions = {
    origin: '*', 
    optionsSuccessStatus: 200 
  }
  
//get config vars
dotenv.config()
const app = express()
const port = 8080

const options = {
  customCssUrl: 'https://unpkg.com/swagger-ui-dist@4.5.0/swagger-ui.css'
};


app.use(cors(corsOptions))
app.use(express.json())
app.use("/api-docs", swaggerUI.serve,swaggerUI.setup(swaggerDocument, options))
app.use("/", router)

app.listen(port, () => {
    console.log(`app listening on port ${port}`)
})